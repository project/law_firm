<?php
/**
 * @file
 * Install, update and uninstall functions for the law profile.
 */

use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\node\Entity\Node;
use \Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function law_install() {
  // Set front page to "home".
  \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/node/25')->save(TRUE);

  // Allow visitor account creation with administrative approval.
  $user_settings = \Drupal::configFactory()->getEditable('user.settings');
  $user_settings->set('register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)->save(TRUE);

  // Enable default permissions for system roles.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, array('access comments'));
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, array(
    'access comments',
    'post comments',
    'skip comment approval',
  ));

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, array('access site-wide contact form'));
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, array('access site-wide contact form'));

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, array('access shortcuts'));

  // Populate the default shortcut set.
  $shortcut = entity_create('shortcut', array(
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => array('uri' => 'internal:/node/add'),
  ));
  $shortcut->save();

  $shortcut = entity_create('shortcut', array(
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => array('uri' => 'internal:/admin/content'),
  ));
  $shortcut->save();

  // Allow all users to use search.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, array('search content'));
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, array('search content'));

  // Enable the admin theme.
  \Drupal::configFactory()->getEditable('node.settings')->set('use_admin_theme', TRUE)->save(TRUE);

  // Create dummy node content.
  $node = Node::create(array(
    'type' => 'page',
    'title' => 'About Us',
    'uid' => '1',
    'status' => 1,
    'path' => '/' . _law_clean_url_alias('About Us'),
  ));
  $node->body->generateSampleItems(1);
  $node->save();

  $home = Node::create(array(
    'type' => 'page',
    'title' => 'Welcome to Law Firm',
    'uid' => '1',
    'status' => 1,
    'path' => '/' . _law_clean_url_alias('home'),
  ));
  $home->save();

  $menu_titles = array(
    'front' => 'Home',
    'about-us' => 'Experiance',
    'latest-news' => 'News',
    'blog' => 'Blog',
    'image-gallery' => 'Image Gallery',
    'video-gallery' => 'Thought Leadership',
    'events-schedule' => 'Events',
    //'team' => 'People',
    'contact' => 'Global Reach',
  );
  $weight = 0;
  // Create menu links for footer menu.
  foreach ($menu_titles as $link => $menu_title) {
    MenuLinkContent::create([
      'title'      => $menu_title,
      'link'       => ['uri' => 'internal:/' . $link],
      'menu_name'  => 'footer',
      'weight'     => $weight,
    ])->save();
    $weight++;
  }

  // Menu links for Header Menu.
  MenuLinkContent::create([
    'title'      => 'Global Reach',
    'link'       => ['uri' => 'internal:/contact'],
    'menu_name'  => 'main',
    'weight'     => 10,
  ])->save();

  MenuLinkContent::create([
    'title'      => 'Experiance',
    'link'       => ['uri' => 'internal:/about-us'],
    'menu_name'  => 'main',
    'weight'     => 2,
  ])->save();

  // Create block for address.
  $address_block = entity_create('block_content', array(
    'info' => 'Contact Us',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '8b024967-e482-42a9-9324-8369a67023ed',
    'body' => array(
      'value' => '<p>Phone: <a href="tel:123456789">123-456-7890</a><br />
      <a href="mailto:info@abc.com">info@abc.com</a><br />
      <br />
     <p>Plot no 29, Sector 18<br /><br />
      Gurgaon, Haryana 122016</p>',
      'format' => 'full_html',
    ),
  ));
  $address_block->save();
  // Create block for facebook.
  $facebook_block = entity_create('block_content', array(
    'info' => 'Facebook Fan',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '125df96d-f3b2-45fe-b026-d7ce4a1a01c4',
    'body' => array(
      'value' => '<p><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fedynamic.net&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=296124383826280" width="290" height="340" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></p>',
      'format' => 'full_html',
    ),
  ));
  $facebook_block->save();

  // Create block for twitter.
  $twitter_block = entity_create('block_content', array(
    'info' => 'Tweet Me',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => 'ae648c58-18e5-4bbe-acc0-7759d4d1e911',
    'body' => array(
      'value' => '<p><a class="twitter-timeline"  href="https://twitter.com/search?q=edynamic" data-widget-id="923123042175160320">Tweets about edynamic</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          </p>',
      'format' => 'full_html',
    ),
  ));
  
  
  $twitter_block->save();
  
  // Create Tickets Block with content.
  $tickets_block = entity_create('block_content', array(
    'info' => 'Contact Us',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '7b024967-e482-42a9-9324-8369a67023ed',
    'body' => array(
      'value' => '<div class="tickets-wrapper">
      <div class="article-text"><span style="text-decoration: underline;"><strong>Head Office Address:</strong></span>
      <p>Plot no 29, Sector 18<br />
      Gurgaon, Haryana 122001<br />
      <br />
      <br />
      <div></div></div>
      
      </div>',
      'format' => 'full_html',
    ),
  ));
  $tickets_block->save();
}
