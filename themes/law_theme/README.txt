About law Theme
====================
law is a Drupal 8 responsive theme.
It has many new features to achieve the functionality.

Features
====================
Implementation of Slideshow.
Social media integration through theme setting.
Copyright Text configuration through theme setting.
Footer with four flexible regions.
Bootstrap 3 framework and Font awesome icons.
Multi-level drop-down menus.

Drupal compatibility:
=====================
This theme is compatible with Drupal 8.x
